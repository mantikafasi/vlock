/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * vlock, a very simple screen locker for Windows
 * Copyright (c) 2023 Vendicated
 */

#pragma once

#include <Windows.h>

#include <string>

#include "util.hpp"

// These names are deliberately slightly obfuscated as Windows Defender was
// flagging us as RAT. Renamed from KeyHook to KHook and it's fixed

struct KHConfig {
    bool ClearCountsAsFail;
    bool RequireEnter;
    int PwLength;
    const wchar_t* Pw;
    void (*OnFail)(const wchar_t* pw);
    void (*OnSuccess)();
    void (*OnInput)(wchar_t inputBuf[], int inputBufLength,
                    const wchar_t* fullInput);
    void (*OnClear)();
};

DLL_EXPORT bool InstallKHook();
DLL_EXPORT bool UninstallKHook();
DLL_EXPORT bool IsKHookInstalled();
DLL_EXPORT void SetKHookConfig(KHConfig config);
