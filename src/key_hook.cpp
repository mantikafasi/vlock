/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * vlock, a very simple screen locker for Windows
 * Copyright (c) 2023 Vendicated
 */

#include "key_hook.hpp"

#include <iostream>
#include <string>

#include "util.hpp"

HHOOK keyboardHook = NULL;
HHOOK mouseHook = NULL;

BYTE keyState[256];
WCHAR buf[256];

KHConfig config;

std::wstring password;
std::wstring actualPassword;

void SetKHookConfig(KHConfig c) {
    config = c;
    // need to copy password as it's possibly invalidated after this for example
    // if you use PInvoke
    actualPassword = std::wstring(config.Pw, config.PwLength);
    config.Pw = actualPassword.c_str();
}

LRESULT CALLBACK KHook(int hookCode, WPARAM wp, LPARAM event) {
    if (hookCode != HC_ACTION)
        return CallNextHookEx(keyboardHook, hookCode, wp, event);

    KBDLLHOOKSTRUCT *kbEvent = reinterpret_cast<KBDLLHOOKSTRUCT *>(event);

    // propagate special keys so they properly work
    switch (kbEvent->vkCode) {
    case VK_CAPITAL:
    case VK_NUMLOCK:
    case VK_SHIFT:
    case VK_LSHIFT:
    case VK_RSHIFT:
        return CallNextHookEx(keyboardHook, hookCode, wp, event);
    }

    // we get a ton of events for each press, so ignore any non key up
    // events
    if ((kbEvent->flags & LLKHF_UP) == 0) {
        return 1;
    }

    switch (kbEvent->vkCode) {
    case VK_ESCAPE:
        password.clear();
        config.OnClear();
        if (config.ClearCountsAsFail) {
            config.OnFail(password.c_str());
        }
        return 1;
    case VK_BACK:
        if (!password.empty()) {
            password.pop_back();

            if (password.empty() && config.ClearCountsAsFail) {
                config.OnFail(password.c_str());
            }
        }
        return 1;
    case VK_RETURN:
        if (password == config.Pw) {
            config.OnSuccess();
        } else {
            config.OnFail(password.c_str());
        }
        config.OnClear();
        password.clear();

        return 1;
    }

    GetKeyboardState(keyState);

    // Decode low level keycode to proper unicode character(s)
    int result = ToUnicodeEx(kbEvent->vkCode, kbEvent->scanCode, keyState, buf,
                             256, 0, GetKeyboardLayout(0));

    if (result > 0) {
        password.append(buf, result);
        config.OnInput(buf, result, password.c_str());

        if (!config.RequireEnter) {
            if (password == config.Pw) {
                config.OnSuccess();
                password.clear();
            } else if (password.length() >= config.PwLength) {
                config.OnFail(password.c_str());
            }
        }
    }

    return 1;
}

LRESULT CALLBACK MHook(int hookCode, WPARAM wp, LPARAM event) { return 1; }

bool IsKHookInstalled() { return keyboardHook != NULL; }

bool InstallKHook() {
    if (!keyboardHook) {
        keyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, KHook,
                                        GetModuleHandle(nullptr), 0);
    }
    if (!mouseHook) {
        mouseHook =
            SetWindowsHookEx(WH_MOUSE_LL, MHook, GetModuleHandle(nullptr), 0);
    }

    return IsKHookInstalled();
}

bool UninstallKHook() {
    if (keyboardHook != NULL) {
        UnhookWindowsHookEx(keyboardHook);
        keyboardHook = NULL;
    }
    if (mouseHook != NULL) {
        UnhookWindowsHookEx(mouseHook);
        mouseHook = NULL;
    }

    return IsKHookInstalled();
}
