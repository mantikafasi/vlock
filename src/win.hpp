/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * vlock, a very simple screen locker for Windows
 * Copyright (c) 2023 Vendicated
 */

#pragma once

#include <Windows.h>

extern HWND win;

void InvalidateWin();
