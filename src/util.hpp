#pragma once

#include <Windows.h>

#include <algorithm>
#include <cctype>
#include <fstream>
#include <locale>
#include <string>

#define DLL_EXPORT extern "C" __declspec(dllexport)

// trim from start (in place)
static inline std::string ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
                return !std::isspace(ch);
            }));
    return s;
}

// trim from end (in place)
static inline std::string rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](unsigned char ch) { return !std::isspace(ch); })
                .base(),
            s.end());
    return s;
}

// trim from both ends (in place)
static inline std::string trim(std::string &s) {
    rtrim(s);
    ltrim(s);
    return s;
}

static inline std::string toLower(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c) { return std::tolower(c); });
    return s;
}

std::wstring stringToWstring(const std::string &string);
