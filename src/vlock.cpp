/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * vlock, a very simple screen locker for Windows
 * Copyright (c) 2023 Vendicated
 */

#ifndef UNICODE
#define UNICODE
#endif

#include <Windows.h>
#include <gdiplus.h>

#include <iostream>

#include "key_hook.hpp"
#include "taskmgr.hpp"
#include "util.hpp"
#include "vars.hpp"
#include "win.hpp"

ULONG_PTR gdiplusToken;

int unlockKeyPresses = 0;
bool failed = false;
HWND win;

HBRUSH bgBrush, failedBrush;

bool usesImages;
Gdiplus::Image* bgImage;
Gdiplus::Image* failImage;

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI wWinMain(HINSTANCE instance, HINSTANCE, PWSTR args, int flag) {
    int exitCode = 0;

    if (!ParseConfig()) return 1;

    if (config.DisableTaskManager) {
        if (!DisableTskMan()) {
            auto message =
                L"Failed to disable Task manager. This is likely "
                L"because you didn't run me as Administrator. Either "
                L"run me as Administrator or set DisableTaskManager to "
                L"false in vlock.conf\n\n"
                L"Proceed anyway?";

            auto choice =
                MessageBoxW(NULL, message, L"Vlock", MB_YESNO | MB_ICONERROR);

            if (choice == IDNO) return 1;
        }
    }

    {
        KHConfig keyConfig;
        keyConfig.PwLength = config.Password.length();
        keyConfig.Pw = (LPWSTR)config.Password.c_str();
        keyConfig.ClearCountsAsFail = config.ClearCountsAsFail;
        keyConfig.RequireEnter = config.RequireEnter;
        keyConfig.OnFail = [](const wchar_t*) {
            if (!failed) {
                failed = true;
                InvalidateWin();
            }
        };
        keyConfig.OnClear = []() {
            if (failed) {
                failed = false;
                InvalidateWin();
            }
        };
        
        keyConfig.OnSuccess = []() { PostQuitMessage(0); };
        keyConfig.OnInput = [](WCHAR input[], int, const wchar_t* fullInput) {};

        SetKHookConfig(keyConfig);

        InstallKHook();
    }

    try {
        auto CLASS_NAME = L"VLOCK";

        WNDCLASS wc = {};
        wc.lpfnWndProc = WindowProc;
        wc.hInstance = instance;
        wc.lpszClassName = CLASS_NAME;
        wc.hbrBackground = NULL;

        RegisterClass(&wc);

        win = CreateWindowEx(0, CLASS_NAME, L"vlock",
                             WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
                             CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                             CW_USEDEFAULT, NULL, NULL, instance, NULL);

        ShowWindow(win, SW_MAXIMIZE);
        SetWindowPos(win, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

        MSG msg = {};
        while (GetMessage(&msg, NULL, 0, 0) > 0) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

    } catch (const std::exception& e) {
        exitCode = 1;
    }

    UninstallKHook();
    if (config.DisableTaskManager) {
        if (!RestoreTskMan()) {
            auto message = L"Failed to re-enable task manager :(";
            MessageBoxW(NULL, message, L"Vlock", MB_OK | MB_ICONWARNING);
        }
    }

    return exitCode;
}

LRESULT CALLBACK WindowProc(HWND win, UINT msg, WPARAM wParam, LPARAM lParam) {
    switch (msg) {
    case WM_CREATE:
        bgBrush = CreateSolidBrush(config.BackgroundColor);
        failedBrush = CreateSolidBrush(config.FailColor);

        usesImages =
            !config.BackgroundImage.empty() || !config.FailImage.empty();
        if (usesImages) {
            Gdiplus::GdiplusStartupInput gdiplusStartupInput;
            Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
        }

        if (!config.BackgroundImage.empty())
            bgImage = new Gdiplus::Image(
                stringToWstring(config.BackgroundImage).c_str());
        if (!config.FailImage.empty())
            failImage =
                new Gdiplus::Image(stringToWstring(config.FailImage).c_str());

        return 0;

    case WM_DESTROY:
        if (usesImages) {
            Gdiplus::GdiplusShutdown(gdiplusToken);
            if (bgImage) delete bgImage;
            if (failImage) delete failImage;
        }

        PostQuitMessage(0);
        return 0;

    case WM_SETCURSOR:
        SetCursor(NULL);
        return 0;

    case WM_PAINT: {
        bool showFail = config.ShowFail && failed;
        if (!config.ShowBackground && !showFail) return 0;

        PAINTSTRUCT paint;
        RECT rect;
        HDC hdc = BeginPaint(win, &paint);
        GetClientRect(win, &rect);

        auto img = showFail ? failImage : bgImage;

        if (img) {
            Gdiplus::Rect r(rect.left, rect.top, rect.right - rect.left,
                            rect.bottom - rect.top);
            Gdiplus::Graphics grpx(hdc);
            grpx.Clear(0x00000000);
            grpx.DrawImage(img, r);
        } else {
            FillRect(hdc, &rect, showFail ? failedBrush : bgBrush);
        }
        EndPaint(win, &paint);

        return 0;
    }
    }

    return DefWindowProc(win, msg, wParam, lParam);
}
