# vlock

A very simple screen locker for Windows.

When opened, draws either a static color (black by default) or an image of your choice on top of all windows and rejects all keyboard / mouse input.

To unlock, enter your specified password and press enter ([configurable](./vlock.conf))

## Building

Install Visual Studio buildtools[^2] (the IDE itself is not needed!) and run scripts/build.ps1.

The build can be found at build/vlock.exe

## Configuration

You can find the default config in [vlock.conf](./vlock.conf)

To edit it, run vlock at least once, then edit %APPDATA%/vlock.conf

## DLL Api

You can also use the vlock backend as a library and implement your own frontend for it. To do so, build the dll with scripts/buildDll.ps1,
and use it as described below

For a usage example, see [the C# example](./api_examples/csharp/Vlock.cs)

The function names are slightly obfuscated as with full names (SetKeyHookConfig instead of SetKHookConfig), Windows Defender was flagging vlock as RAT (lol)

```cpp
struct KHConfig {
    // Whether Clearing the input counts as a failed attempt
    bool ClearCountsAsFail;
    // Whether pressing enter is required to confirm a password attempt
    bool RequireEnter;
    // The length of the password
    int PwLength;
    // The password
    const wchar_t* Pw;
    // A callback that will be invoked on a failed unlock attempt. The "pw" argument is the attempted password.
    // This is potentially an empty string if ClearCountsAsFail is true
    void (*OnFail)(const wchar_t* pw);
    // A callback that will be invoked on a successful unlock attempt
    void (*OnSuccess)();
    // A callback that will be invoked per character typed
    // inputBuf is a buffer that contains one or more characters corresponding to this keypress.
    // inputBufLength is a number indicating how many elements inputBuf contains. Reading anything past this element count leads to undefined behaviour
    // fullInput is the full input
    void (*OnInput)(wchar_t inputBuf[], int inputBufLength,
                    const wchar_t* fullInput);
};

// Install the Keyboard Hook: Start listening to input
DLL_EXPORT bool InstallKHook();
// Uninstall the Keyboard Hook
DLL_EXPORT bool UninstallKHook();
// Whether the Keyboard hook is currently installed
DLL_EXPORT bool IsKHookInstalled();
// Sets the current KHConfig. You MUST call this at least once before calling InstallKHook
DLL_EXPORT void SetKHookConfig(KHConfig config);

// Disable the TaskManager
DLL_EXPORT bool DisableTskMan();
// Enable the TaskManager
DLL_EXPORT bool RestoreTskMan();
```

You should treat the arguments passed to your callback as immutable. Mutating them is not allowed.

Using any of the arguments passed to your callbacks after your callback has returned is not allowed.
If you want to keep them around, make a copy

## TODO

- [x] Ability to use an image as background
- [ ] Ability to blur the screen (bonus points for customisable blur amount)
- [x] Proper password instead of single character
- [x] Move config from header to something like json / yaml
- [x] Figure out a way to prevent task manager shortcut
- [x] Add a way to find out if someone tried to use your PC (turn background red, log to file, messagebox once unlocked, etc)

## License

Copyright (c) 2023 Vendicated
GPL 3.0 or later

[^1]: Ctrl+Alt+Delete is not intercepted as Windows doesn't allow it ☹️ However, vlock disables the task manager so you can't abuse it.
[^2]: Visit the [Download Page](https://visualstudio.microsoft.com/downloads/), scroll down to All Downloas > Tools for Visual Studio and download Build Tools for Visual Studio 2022

Also whoever tf designed the windows api needs to be put down it's SOOOOOO BAD
