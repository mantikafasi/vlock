. ./scripts/init.ps1

$sources = "src/vlock.cpp", "src/key_hook.cpp", "src/win.cpp", "src/config.cpp", "src/taskmgr.cpp", "src/util.cpp"

# /Fo: Output object (.obj) file location
# /Fe: Output executable (.exe) location
# Link user32.lib
cl $sources /EHsc /std:c++20 $libs /Fo"build\" /Fe"build\"
